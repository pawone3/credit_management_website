<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
	<style>
		.info-wrap {
			text-align: center;
		}
        .w3-top{
            margin-bottom: 200%;
        }
        body {
            background-color: #EABEB5;
            opacity: 0.7;
        }
        .temp{
            height: 40px;
        }
	</style>
	</head>
	<body>

        <div class="w3-top">
            <div class="w3-bar w3-white w3-card" id="myNavbar">
                <a href="#home" class="w3-bar-item w3-button w3-wide">The Sparks Foundation</a>
                <div class="w3-right w3-hide-small">
                    <a href="https://www.thesparksfoundationsingapore.org/" class="w3-bar-item w3-button">ABOUT US</a>
                </div>
                </a>
            </div>
        </div>
        <div class="temp"></div>
		<div class="container">
			<div class="info-wrap">
			  <h1 class="user-name">Username : <?php echo $data->username?></h1>
			  <h1 class="user-name">Name : <?php echo $data->name?></h1>
			   <p>Email : <?php echo $data->email?></p>
			   <p>Current Credits : <?php echo $data->current_credit?></p>
			   <a href="<?php echo base_url();?>index.php/Users/select_debtor/<?php echo $data->id?>">Transfer Credits</a>
			</div>
		</div>
	</body>
</html>
