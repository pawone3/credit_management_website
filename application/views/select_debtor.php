<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/util.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .headd {
            text-align: center;
            color: black;
            font-size: 30px ;
            margin-bottom: 10px;
        }

        .temp{
            height: 30px;
            font-size: 140%;
        }
        .temp{
            height: 10px;
        }
        .w3-top{
            font-size: 140%;
            /*opacity: ;*/
        }
        body {
            background-color: #EABEB5;
            opacity: 0.7;

        }
       .tab1{
           width: 100%;
           min-height: 100vh;
           background: #EABEB5;
           display: -webkit-box;
           display: -webkit-flex;
           display: -moz-box;
           display: -ms-flexbox;
           display: flex;
           align-items: center;
           justify-content: center;
           flex-wrap: wrap;
           padding: 33px 30px;
       }

    </style>
</head>
<body>

<div class="w3-top ">
    <div class="w3-bar w3-white w3-card" id="myNavbar">
        <a href="#home" class="w3-bar-item w3-button w3-wide">The Sparks Foundation</a>
        <div class="w3-right w3-hide-small">
            <a href="https://www.thesparksfoundationsingapore.org/" class="w3-bar-item w3-button">ABOUT US</a>
        </div>
        </a>
    </div>
</div>
<div class="temp"></div>
<div class="limiter">
		<div class="container-table100 tab1">
			<div class="wrap-table100">
                <div class="headd">
                    Selector Debtor
                </div>
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Name</th>
								<th class="column2">Username</th>
								<th class="column3">Email</th>
                                <th class="column42">Current Credits</th>
								<th class="column41">Send Credits</th>
							</tr>
						</thead>
						<tbody>
						
						
						<?php foreach($data as $dataa):
                            if(($dataa->id)!=$creditor_id):
                            ?>
							<tr>
								<h2><td class="column1"><?php echo $dataa->name?></td></h2>
								<h4><td class="column2"><?php echo $dataa->username?></td></h4>
								<td class="column3"><?php echo $dataa->email?></td>
                                <td class="column42"><?php echo $dataa->current_credit?></td>
								<td class="column41"><button class="button" onclick="take_input(<?php echo $dataa->id; ?>);">Select </button></td>

							</tr>
                        <?php
                        endif;
						endforeach; ?>
							

						</tbody>
					</table>
				</div>
			</div>
            <div class="td5" id="td5"></div>
		</div>

	</div>
	<script type="text/javascript">
		
		function take_input(debtor_id){

			var td5=document.getElementById('td5');
			td5.innerHTML='<form method ="post" action="<?php echo base_url();?>index.php/Users/transfer">\n' +
                '      <label for="choose"><h2>Enter Credit Amount :</h2></label>\n' +
                '      <input class="amount" id="choose" size"20" name="amount" required>\n' +
                '      <input id="debt" name="debtor_id" type="hidden" value="" required>\n' +
                '      <button class="button1">Submit</button>\n' +
                '    </form>\n';
			var elm=document.getElementById("debt");
			debt.setAttribute("value",debtor_id);
		}

	</script>
</body>
</html>
