<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/main.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/util.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        .headd {
            text-align: center;
            color: black;
            font-size: 30px ;
            margin-bottom: 10px;
        }
        .w3-top{
            margin-bottom: 150%;
        }
        body {
            background-color: #EABEB5;
            opacity: 0.7;
        }
        .tab1{
            width: 100%;
            min-height: 100vh;
            background: #EABEB5;
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            padding: 33px 30px;
        }
    </style>
</head>
<body>

<div class="w3-top container">
    <div class="w3-bar w3-white w3-card" id="myNavbar">
        <a href="#home" class="w3-bar-item w3-button w3-wide">The Sparks Foundation</a>
        <div class="w3-right w3-hide-small">
            <a href="https://www.thesparksfoundationsingapore.org/" class="w3-bar-item w3-button">ABOUT US</a>
        </div>
        </a>
    </div>
</div>
	
	<div class="limiter">
		<div class="container-table100 tab1">
			<div class="wrap-table100">
                <div class="headd">
                    Users List
                </div>
				<div class="table100">
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Name</th>
								<th class="column2">Username</th>
								<th class="column3">Available Credits</th>
                                <th class="column4">View Profile</th>
								<th class="column5">Email</th>
                                <th class="column6">Transfer Credits</th>

							</tr>
						</thead>
						<tbody>
						
						
						<?php foreach($data as $dataa): ?>
							<tr>
								<h2><td class="column1"><?php echo $dataa->name?></td></h2>
								<h4><td class="column2"><?php echo $dataa->username?></td></h4>
								<td class="column3"><?php echo $dataa->current_credit?></td>
                                <td class="column4"><a href="<?php echo base_url();?>index.php/Users/select_user/<?php echo $dataa->id?>">View Profile</a></td>
								<td class="column5"><?php echo $dataa->email?></td>
                                <td class="column6"><a href="<?php echo base_url();?>index.php/Users/select_debtor/<?php echo $dataa->id?>">Transfer Credits</a></td>

							</tr>
						<?php endforeach; ?>
							

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
