-- ------- --------------------------------------------------- SQL FILE FOR CREDIT MANAGEMENT WEBSITE --------- --
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 21, 2019 at 12:10 PM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.1
-- -------  SQL FILE FOR CREDIT MANAGEMENT WEBSITE --------- --

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `credit`
--

-- --------------------------------------------------------

--
-- Table structure for table `Transfers`
--

CREATE TABLE `Transfers` (
  `id` int(15) NOT NULL,
  `creditor_id` int(11) NOT NULL,
  `debtor_id` int(11) NOT NULL,
  `creditor_name` varchar(25) NOT NULL,
  `debtor_name` varchar(25) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_transferred` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Transfers`
--

INSERT INTO `Transfers` (`id`, `creditor_id`, `debtor_id`, `creditor_name`, `debtor_name`, `time`, `amount_transferred`) VALUES
(14, 1, 3, 'maxwell', 'zampa', '2019-03-21 11:35:54', 22),
(15, 2, 4, 'handscomb', 'bumrah', '2019-03-21 11:37:35', 100),
(16, 2, 3, 'handscomb', 'zampa', '2019-03-21 11:37:51', 22),
(17, 2, 4, 'handscomb', 'bumrah', '2019-03-21 11:38:08', 22),
(18, 4, 1, 'bumrah', 'maxwell', '2019-03-21 11:38:20', 40),
(19, 1, 3, 'maxwell', 'zampa', '2019-03-21 11:39:30', 22);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `current_credit` int(14) NOT NULL DEFAULT '1000',
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `name`, `email`, `current_credit`, `username`) VALUES
(1, 'maxwell', 'max@123.com', 20, 'max'),
(2, 'handscomb', 'handy@123', 942, 'handy'),
(3, 'zampa', 'zamp@123', 1715, 'zamp'),
(4, 'bumrah', 'bumbo@123', 1400, 'bumbo'),
(5, 'braithwaite', 'braith@123', 1000, 'brat'),
(6, 'holder', 'holder@123', 1000, 'holdy'),
(7, 'sam billings', 'billing@123', 1000, 'billy'),
(8, 'pat cummins', 'pat@123', 1000, 'patty');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Transfers`
--
ALTER TABLE `Transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Transfers`
--
ALTER TABLE `Transfers`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
